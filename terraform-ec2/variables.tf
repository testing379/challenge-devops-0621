variable "project_name" {}
variable "vpc_cidr" {
    default = "172.31.0.0/16"
}

variable "ami_id" {}