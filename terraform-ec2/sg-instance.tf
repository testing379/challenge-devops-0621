resource "aws_security_group" "app-server" {
  name = "appserver"
  tags = {
    Name = "App-srv-${var.project_name}"
    project = "${var.project_name}"
  }

ingress {
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = ["${var.vpc_cidr}","11.22.33.44/32"]
}
}