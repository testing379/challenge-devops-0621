# Creación de instancia en EC2 con terraform


En este directorio tenemos todos los archivos necesarios para crear una instancia ec2 en AWS 


### requisitos :

* credencial activa y funcional en el profile default ~/.aws/credentials

`````
cat ~/.aws/config
[default]
profile = default
aws_access_key_id = ACCESSKEY_AWS
aws_secret_access_key = SECRET_KEY_AWS
region = us-west-2
output = json
`````
* que exista una DEFAULT VPC en la cuenta AWS

## Descripción de archivos 


* main.tf : configuración principal instancia y provider de terraform. 
* sg-instance.tf : configuración security group de la instancia.  
* terraform.tfvars : Valores de las variables utilizadas en la configuración. 
* variables.tf : declaración de las variables que se utilizaran en la configuración. 