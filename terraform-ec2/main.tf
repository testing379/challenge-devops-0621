terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-west-2"
}


resource "aws_instance" "app-server" {
  ami           = "${var.ami_id}"
  vpc_security_group_ids  = ["${aws_security_group.app-server.id}"]
  instance_type = "t3a.nano"
#   root_block_device = {
#         volume_type = "gp2"
#         volume_size = 20
#         delete_on_termination = true            
#   }

  tags = {
    Name = "app-srv-${var.project_name}"
    project = "${var.project_name}"
  }
}
